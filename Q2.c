#include<stdio.h>
int main()
{
	int n, i;
	char prime = 'y';
	printf("\nEnter a positive integer : ");
	scanf("%d", &n);
	for(i = 2 ; i <= n/2 ; ++i)
	{
		if(n % i == 0)
		{
			prime = 'n';
			break;
		}
	}
	if(prime == 'y')
	{
		printf("\n%d is a prime number.\n", n);
	}
	else
	{
		printf("\n%d is not a prime number.\n", n);
	}
	return 0;
}
